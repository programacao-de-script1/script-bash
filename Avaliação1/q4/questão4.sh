#!/bin/bash

echo "### Informações da CPU ###"
lscpu | grep "Model name\|Architecture\|CPU(s)"

echo "### Informações da Memória RAM ###"
grep "MemTotal" /proc/meminfo

echo "### Informações sobre os Discos ###"
df -h | grep "Filesystem\|Size\|Used\|Avail\|Mounted on"

echo "### Informações sobre a Placa de Vídeo ###"
lspci | grep VGA

echo "### Informações sobre Placas de Rede ###"
ip a | grep "^[0-9]\|inet "

echo "### Informações sobre o Sistema Operacional ###"
uname -a

neofetch
