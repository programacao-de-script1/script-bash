#!/bin/bash

v1="Isso é uma variável"    
echo "Minha variável contém: $v1"

v1="Novo valor"
echo "Minha variável agora contém: $v1"

echo "A variável de ambiente HOME contém: $HOME"

echo "Digite seu nome: "
read n1  
echo "Você digitou: $n1"

echo "O primeiro argumento da linha de comando é: $1"
echo "O segundo argumento da linha de comando é: $2"

echo "PID do script: $$"
echo "Número de argumentos passados: $#"
echo "Todos os argumentos passados: $@"
echo "Último status de saída: $?"

