#!/bin/bash

echo "Digite o nome do primeiro diretório:"
read -r d1
echo "Digite o nome do segundo diretório:"
read -r d2

echo "Digite o nome do terceiro diretório:"
read -r d3

echo "Diretório: $d1"
echo "arquivos .xls: $(ls $d1 | grep -cE '\.xls$')"
echo "arquivos .bmp: $(ls $d1 | grep -cE '\.bmp$')"
echo "arquivos .docx: $(ls $d1 | grep -cE '\.docx$')"

echo "Diretório: $d2"
echo "arquivos .xls: $(ls $d2 | grep -cE '\.xls$')"
echo "arquivos .bmp: $(ls $d2 | grep -cE '\.bmp$')"
echo "arquivos .docx: $(ls $d2 | grep -cE '\.docx$')"

echo "Diretório: $d3"
echo "arquivos .xls $(ls $d3 | grep -cE '\.xls$')"
echo "arquivos .bmp: $(ls $d3 | grep -cE '\.bmp$')"
echo "arquivos .docx: $(ls $d3 | grep -cE '\.docx$')"
