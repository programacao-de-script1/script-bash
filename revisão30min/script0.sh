#!/bin/bash

echo "1. \$0: Nome do script atual - $0"

echo "2. \$#: Número de argumentos passados - $#"

echo "3. \$1: Primeiro argumento - $1"
echo "   \$2: Segundo argumento - $2"

echo "4. \$@: Todos os argumentos - \$@"

echo "5. \$\$: PID do script - $$"

echo "6. \$?: Código de retorno do último comando - $?"

echo "7. \$!: PID do último processo em segundo plano - $!"

echo "8. \$*: Todos os argumentos como uma única string - \$*"

echo "9. \$-: Opções atuais do shell - $-"

echo "10. \$USER: Nome do usuário atual - $USER"

echo "11. \$HOME: Diretório home do usuário - $HOME"

echo "12. \$PWD: Diretório atual de trabalho - $PWD"

echo "13. \$HOSTNAME: Nome do host da máquina - $HOSTNAME"

echo "14. \$SHELL: Caminho para o shell atual - $SHELL"

echo "15. \$RANDOM: Número aleatório - $RANDOM"

echo "16. \$LINENO: Número da linha atual - $LINENO"

echo "17. \$SECONDS: Número de segundos desde o início do script - $SECONDS"

echo "18. \$0: Nome do shell/script pai - $0"

function example_function() {
    echo "19. \$FUNCNAME: Nome da função atual - $FUNCNAME"
}
example_function

echo "20. \$BASH_VERSION: Versão do Bash - $BASH_VERSION"
