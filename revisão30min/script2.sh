#!/bin/bash

somar_numeros() {
  python3 -c "print($1 + $2)"
}

echo "Digite o primeiro número:"
read a

echo "Digite o segundo número:"
read b

Resultado=$(somar_numeros $a $b)

echo "Resultado: $a + $b = $Resultado"
