#!/bin/bash

if [ "$#" -ne 1 ]; then
  echo "Uso: $0 <caminho_da_pasta>"
  exit 1
fi

pasta="$1"

if [ ! -d "$pasta" ]; then
  echo "A pasta '$pasta' não existe."
  exit 1
fi

arquivos=$(find "$pasta" -type f | wc -l)
diretorios=$(find "$pasta" -type d | wc -l)

echo "Na pasta '$pasta':"
echo "Quantidade de arquivos: $arquivos"
echo "Quantidade de diretórios: $diretorios"
