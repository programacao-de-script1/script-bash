#!/bin/bash

if [ "$EUID" -ne 1000 ]; then
  echo "Este script deve ser executado como o usuário 'ifpb'."
  exit 1
fi

if [ "$#" -eq 0 ]; then
  pasta="/home/ifpb"
else
  pasta="$1"
fi

if [ ! -d "$pasta" ]; then
  echo "A pasta '$pasta' não existe."
  exit 1
fi

arquivos=$(find "$pasta" -type f | wc -l)
diretorios=$(find "$pasta" -type d | wc -l)

echo "Na pasta '$pasta':"
echo "Quantidade de arquivos: $arquivos"
echo "Quantidade de diretórios: $diretorios"
